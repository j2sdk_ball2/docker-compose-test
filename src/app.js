const http = require('http')

module.exports = () => {
    const handleRequest = (request, response) => {
        console.log('Received request for URL: ' + request.url)
        response.writeHead(200)
        response.end('Server v13')
    }
    const www = http.createServer(handleRequest)
    const listener = www.listen(5000)
    return listener
}